const { gql } = require("apollo-server");

module.exports = gql`
  type Order {
    items: [OrderItem!]!
    deliveryAddress: DeliveryAddress!
    deliveryDueDate: String
    actualDeliveryDate: String
    status: String
    deleted: Boolean
    createdAt: String
    updatedAt: String
  }

  type Product {
    title: String
    shortDescrip: String
    descrip: String
    pricing: Float
    stock: Int
    photos: [productImage]
    specs: [productSpec]
    supplier: Supplier!
    deleted: Boolean
    createdAt: String
    updatedAt: String
  }

  type Supplier {
    name: String
    logo: String
    contactEmail: String
    contactPhone: String
    isActive: Boolean
    createdAt: String
    updatedAt: String
  }

  type DeliveryAddress {
    title: String
    address: String
    createdAt: String
    updatedAt: String
  }

  type OrderItem {
    product: Product!
    quantity: Int! # Use Int for quantity
  }

  type productImage {
    imgURL: String
    alt: String
  }

  type productSpec {
    title: String
    descrip: String
  }

  input ProductImageInput {
    imgURL: String!
    alt: String
  }

  input ProductSpecInput {
    title: String!
    descrip: String
  }

  input ProductInOrderInput {
    product: ID!
    quantity: Int!
  }

  input ProductInput {
    title: String
    shortDescrip: String
    descrip: String
    pricing: Float
    stock: Int
    photos: [ProductImageInput]
    specs: [ProductSpecInput]
    supplier: ID!
  }

  input DeliveryAddressInput {
    title: String!
    address: String!
  }

  input OrderInput {
    items: [ProductInOrderInput!]!
    deliveryAddress: ID!
    deliveryDueDate: String!
    actualDeliveryDate: String
    status: String
  }

  input SupplierInput {
    name: String!
    logo: String!
    contactEmail: String
    contactPhone: String
  }

  type Query {
    order(ID: ID!): Order!
    getOrders(amount: Int): [Order]

    product(ID: ID!): Product!
    getProducts(amount: Int): [Product]

    deliveryAddress(ID: ID!): DeliveryAddress!
    getDeliveryAddresses(amount: Int): [DeliveryAddress]

    supplier(ID: ID!): Supplier!
    getSuppliers(amount: Int): [Supplier]
  }

  type Mutation {
    createOrder(orderInput: OrderInput!): Order!
    deleteOrder(ID: ID!): Boolean
    editOrder(ID: ID!, orderInput: OrderInput): Order!

    createProduct(productInput: ProductInput!): Product!
    deleteProduct(ID: ID!): Boolean
    editProduct(ID: ID!, productInput: ProductInput): Boolean

    createDeliveryAddress(addressInput: DeliveryAddressInput!): DeliveryAddress!
    deleteDeliveryAddress(ID: ID!): Boolean
    editDeliveryAddress(ID: ID!, addressInput: DeliveryAddressInput): Boolean

    createSupplier(supplierInput: SupplierInput!): Supplier!
    changeSupplierStatus(ID: ID!): Supplier
    editSupplier(ID: ID!, supplierInput: SupplierInput): Boolean
  }
`;
