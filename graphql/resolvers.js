const ProductResolver = require("./resolvers/productResolver");
const OrderResolver = require("./resolvers/orderResolver");
const AddressResolver = require("./resolvers/deliveryAddressResolver");
const SupplierResolver = require("./resolvers/supplierResolver");

module.exports = {
  Query: {
    ...ProductResolver.Query,
    ...OrderResolver.Query,
    ...AddressResolver.Query,
    ...SupplierResolver.Query,
  },
  Mutation: {
    ...ProductResolver.Mutation,
    ...OrderResolver.Mutation,
    ...AddressResolver.Mutation,
    ...SupplierResolver.Mutation,
  },
};
