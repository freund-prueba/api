const Supplier = require("../../models/Supplier");

module.exports = {
  Query: {
    async supplier(_, { ID }) {
      return await Supplier.findById(ID);
    },
    async getSuppliers(_, { amount }) {
      return await Order.find().sort({ createdAt: -1 }).limit(amount);
    },
  },
  Mutation: {
    async createSupplier(_, { supplierInput }) {
      const createdSupplier = new Supplier({
        ...supplierInput,
        isActive: true,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      });

      const savedSupplier = await createdSupplier.save();

      return {
        id: savedSupplier.id,
        ...savedSupplier._doc,
      };
    },
    async changeSupplierStatus(_, { ID }) {
      const supplier = await Supplier.findById(ID);
      const supplierIsActive = supplier.isActive;
      const updatedSupplier = await Supplier.findByIdAndUpdate(
        ID,
        { isActive: !supplierIsActive, updatedAt: new Date().toISOString() },
        { new: true } // Return the updated document
      );

      if (!updatedSupplier) {
        throw new Error("Supplier not found");
      }

      return updatedSupplier;
    },

    async editSupplier(_, { ID, supplierInput }) {
      const wasEdited = (
        await Supplier.updateOne(
          { _id: ID },
          { ...supplierInput, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasEdited;
    },
  },
};
