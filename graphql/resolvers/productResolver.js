const Product = require("../../models/Product");

module.exports = {
  Query: {
    async product(_, { ID }) {
      return await Product.findById(ID).populate("supplier");
    },
    async getProducts(_, { amount }) {
      return await Product.find()
        .populate("supplier")
        .sort({ createdAt: -1 })
        .limit(amount);
    },
  },
  Mutation: {
    async createProduct(_, { productInput }) {
      const createdProduct = new Product({
        ...productInput,
        deleted: false,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      });

      const savedProduct = await createdProduct.save();
      const populatedProduct = await Product.findById(
        savedProduct._id
      ).populate("supplier");

      return populatedProduct;
    },
    async deleteProduct(_, { ID }) {
      const wasDeleted = (
        await Product.updateOne(
          { _id: ID },
          { deleted: true, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasDeleted;
    },

    async editProduct(_, { ID, productInput }) {
      const wasEdited = (
        await Product.updateOne(
          { _id: ID },
          { ...productInput, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasEdited;
    },
  },
};
