const Address = require("../../models/DeliveryAddress");

module.exports = {
  Query: {
    async deliveryAddress(_, { ID }) {
      return await Address.findById(ID);
    },
    async getDeliveryAddresses(_, { amount }) {
      return await Address.find().sort({ createdAt: -1 }).limit(amount);
    },
  },
  Mutation: {
    async createDeliveryAddress(_, { addressInput: { title, address } }) {
      const createdDeliveryAddress = new Address({
        title,
        address,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      });

      const res = await createdDeliveryAddress.save();

      return {
        id: res.id,
        ...res._doc,
      };
    },
    async deleteDeliveryAddress(_, { ID }) {
      const wasDeleted = (await Address.deleteOne({ _id: ID })).deletedCount;
      return wasDeleted;
    },

    async editDeliveryAddress(_, { ID, addressInput }) {
      const wasEdited = (
        await Address.updateOne(
          { _id: ID },
          { ...addressInput, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasEdited;
    },
  },
};
