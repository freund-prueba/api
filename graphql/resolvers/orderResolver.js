const Order = require("../../models/Order");
const STATUS = require("../../consts");

module.exports = {
  Query: {
    async order(_, { ID }) {
      return await Order.findById(ID)
        .populate("items.product")
        .populate("deliveryAddress");
    },
    async getOrders(_, { amount }) {
      return await Order.find()
        .populate("items.product")
        .populate("deliveryAddress")
        .sort({ createdAt: -1 })
        .limit(amount);
    },
  },
  Mutation: {
    async createOrder(_, { orderInput }) {
      const createdOrder = new Order({
        ...orderInput,
        status: STATUS.PENDING,
        deleted: false,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      });

      const savedOrder = await createdOrder.save();
      const populatedOrder = await Order.findById(savedOrder._id)
        .populate("items.product")
        .populate("deliveryAddress");

      return populatedOrder;
    },
    async deleteOrder(_, { ID }) {
      const wasDeleted = (
        await Order.updateOne(
          { _id: ID },
          { deleted: true, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasDeleted;
    },

    async editOrder(_, { ID, orderInput }) {
      const wasEdited = (
        await Order.updateOne(
          { _id: ID },
          { ...orderInput, updatedAt: new Date().toISOString() }
        )
      ).modifiedCount;
      return wasEdited;
    },
  },
};
