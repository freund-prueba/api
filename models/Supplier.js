const { model, Schema } = require("mongoose");

const supplierSchema = new Schema({
  name: String,
  logo: String,
  contactEmail: String,
  contactPhone: String,
  isActive: Boolean,
  createdAt: String,
  updatedAt: String,
});

module.exports = model("Supplier", supplierSchema);
