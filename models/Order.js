const { model, Schema } = require("mongoose");

const productInOrderSchema = new Schema({
  product: { type: Schema.Types.ObjectId, ref: "Product", required: true },
  quantity: { type: Number, required: true, min: 1 },
});

const orderSchema = new Schema({
  title: String,
  items: [productInOrderSchema],
  deliveryAddress: {
    type: Schema.Types.ObjectId,
    ref: "DeliveryAddress",
    required: true,
  },
  supplierNote: String,
  deliveryDueDate: String,
  actualDeliveryDate: String,
  deliveryCost: Number,
  taxes: Number,
  totalCost: Number,
  status: String,
  deleted: Boolean,
  createdAt: String,
  updatedAt: String,
});

module.exports = model("Order", orderSchema);
