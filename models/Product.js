const { model, Schema } = require("mongoose");

const productImageSchema = new Schema({
  imgURL: String,
  alt: String,
});

const productSpecSchema = new Schema({
  title: String,
  descrip: String,
});

const productSchema = new Schema({
  title: String,
  shortDescrip: String,
  descrip: String,
  pricing: Number,
  stock: Number,
  supplier: { type: Schema.Types.ObjectId, ref: "Supplier", required: true },
  photos: [productImageSchema],
  specs: [productSpecSchema],
  deleted: Boolean,
  createdAt: String,
  updatedAt: String,
});

module.exports = model("Product", productSchema);
