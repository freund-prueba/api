const { model, Schema } = require("mongoose");

const deliveryAddressSchema = new Schema({
  title: String,
  address: String,
  createdAt: String,
  updatedAt: String,
});

module.exports = model("DeliveryAddress", deliveryAddressSchema);
