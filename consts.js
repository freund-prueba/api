/* pending: The order has been submitted but not yet processed by the seller.
processing: The seller has received the order and is working on it (e.g., preparing the items, packing them).
shipped: The order has been shipped by the seller and is on its way to the buyer.
delivered: The order has been delivered to the buyer.
cancelled: The order has been cancelled, either by the buyer or the seller. */

const STATUS_ITEMS = {
  PENDING: "pending",
  PROCESSING: "processing",
  SHIPPED: "shipped",
  DELIVERED: "delivered",
  CANCELLED: "cancelled",
};

module.exports = STATUS_ITEMS;
